#upgrading linux kernel
apt-cache search linux-image
sudo apt-get install linux-image-xxxxxxxx

****************** error: locale.Error: unsupported locale setting
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure locales

#updating the repository of apt-get
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update

#installing pip
sudo apt-get install python-pip

#clone the required repositories
git clone https://mtajiki@bitbucket.org/mtajiki/pyroute2.git
git clone https://mtajiki@bitbucket.org/nip1617/grpc-seg-rout.git

#installing iproute2
wget https://www.kernel.org/pub/linux/utils/net/iproute2/iproute2-4.15.0.tar.gz
tar -xvzf iproute2-4.15.0.tar.gz
cd iproute2-4.15.0/
make
sudo make install

sudo pip install --upgrade pip

#installing required package
sudo pip install protobuf
sudo pip install psutil
sudo pip install grpcio
sudo pip install future
